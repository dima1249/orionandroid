package bright.orion.app;

/**
 * Created by orgil on 12/12/2017.
 */

public class Const {
    public static final String PREF_FILE_NAME = "LOGIN_SESSION";
    public static final String KEY_ERROR_MESSAGE = "ERROR_MSG";
    public static final String ARG_LOGIN = "ARG_LOGIN";
    public static final String SUCCESS_RES = "SUCCESS_RES";
    public static final String USER_NAME = "USER_NAME";
    public static final String PASSWORD = "PASSWORD";
    public final static String LOGOUT = "LOGOUT";
    public final static String IP = "IP";
    public final static String DB = "DB";
    public final static String USER = "USER";
    public final static String DBPASSWORD = "DBPASSWORD";
}
