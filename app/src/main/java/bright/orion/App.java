package bright.orion;

import android.app.Application;
import android.content.Context;

import java.sql.Connection;

import bright.orion.rest.ConnectionClass;

/**
 * Created by orgil on 12/12/2017.
 */

public class App extends Application {

    private static Connection connection;

    public static Connection getConnection() {
        return connection;
    }

    public static void reConnect(Context context) {
        connection = new ConnectionClass().CONN(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        connection = new ConnectionClass().CONN(getApplicationContext());
    }
}
