package bright.orion.rest;

/**
 * Created by orgil on 12/10/2017.
 */

public class Queries {

    public static String login(String name, String pass) {
        return "select\n" +
                "\tu.UserID,\n" +
                "\tu.LoginName user_name\n" +
                "from\n" +
                "\ttUser u with (nolock)\n" +
                "where\n" +
                "\tu.LoginName = '"+name+"' and\n" +
                "\tCONVERT(VARBINARY(250), LTRIM(RTRIM(u.Password))) = CONVERT(VARBINARY(250), LTRIM(RTRIM('"+pass+"')))";
    }

    public static String getCounts() {
        return "select\n" +
                "\tc.full_id,\n" +
                "\tc.full_id + ', ' + convert(varchar(20), c.[date], 111) + ': ' + w.name toollogo_name,\n" +
                "\tN'Төлөв: ' + s.name [state],\n" +
                "\ts.code state_code,\n" +
                "\tl.total_sale_price\n" +
                "from\n" +
                "\tsto_counting c inner join\n" +
                "\t(\n" +
                "\t\tselect\n" +
                "\t\t\tc.full_id,\n" +
                "\t\t\tsum(l.qty * isnull(l.sale_price, pp.sale_price)) total_sale_price\n" +
                "\t\tfrom\n" +
                "\t\t\tsto_counting c with (nolock) left join\n" +
                "\t\t\tsto_counting_line l with (nolock) on c.full_id = l.full_id left join\n" +
                "\t\t\tsto_product p with (nolock) on l.group_num = p.group_num left join\n" +
                "\t\t\tsto_product_price pp with (nolock) on\n" +
                "\t\t\t\tl.group_num = pp.group_num and\n" +
                "\t\t\t\tpp.deleteddate is null and\n" +
                "\t\t\t\tc.warehouse_id = pp.warehouse_id\n" +
                "\t\twhere\n" +
                "\t\t\tp.deleteddate is null and\n" +
                "\t\t\t(c.[date] >= ? or convert(varchar(20), c.[date], 111) = convert(varchar(20), ?, 111)) and\n" +
                "\t\t\t(c.[date] <= ? or convert(varchar(20), c.[date], 111) = convert(varchar(20), ?, 111))\n" +
                "\t\tgroup by c.full_id\n" +
                "\t) l on c.full_id = l.full_id inner join\n" +
                "\tsto_warehouse w with (nolock) on c.warehouse_id = w.id inner join\n" +
                "\tsto_counting_state s on c.state_id = s.id\n" +
                "order by c.[date] desc\n";
    }

    public static String getProductByBarCode() {
        return "select b.bar_code, p.group_num, p.name, c.name category, pp.sale_price\n" +
                "from\n" +
                "\tsto_product p with (nolock) inner join\n" +
                "\tsto_product_barcode b with (nolock) on p.group_num = b.group_num left join\n" +
                "\tsto_product_category c with (nolock) on p.categ_id = c.id left join\n" +
                "\tsto_product_price pp with (nolock) on\n" +
                "\t\tp.group_num = pp.group_num and\n" +
                "\t\tpp.deleteddate is null and\n" +
                "\t\tpp.warehouse_id = (select warehouse_id from sto_counting where full_id = ?)\n" +
                "order by b.bar_code;";
    }

    public static String saveCount(String f_id,
                                   String bar_code,
                                   String qty,
                                   String group_num,
                                   String sale,
                                   String user_id,
                                   String date
                                   ) {
        return "BEGIN TRAN;\n" +
                "\tUPDATE Sto_Counting \n" +
                "\tSET last_updated_date = getdate() \n" +
                "\tWHERE\n" +
                "\t\tfull_id = '"+f_id+"';\n" +
                "\tIF\n" +
                "\t\t@@ERROR <> 0 BEGIN\n" +
                "\t\t\tROLLBACK TRAN;\n" +
                "\t\tRETURN;\n" +
                "\t\t\n" +
                "\tEND DELETE \n" +
                "\tFROM\n" +
                "\t\tsto_counting_line \n" +
                "\tWHERE\n" +
                "\t\tbar_code = '"+bar_code+"' \n" +
                "\t\tAND full_id = '"+f_id+"';\n" +
                "\tIF\n" +
                "\t\t@@ERROR <> 0 BEGIN\n" +
                "\t\t\tROLLBACK TRAN;\n" +
                "\t\tRETURN;\n" +
                "\t\t\n" +
                "\tEND INSERT INTO sto_counting_line ( full_id, group_num, bar_code, qty, user_id, sale_price, systemdate ) " +
                "SELECT\n" +
                "\t'"+f_id+"',\n" +
                "\t"+group_num+",\n" +
                "\t'"+bar_code+"',\n" +
                "\t"+qty+",\n" +
                "\t"+user_id+",\n" +
                "\t"+sale+",\n" +
                "\t'"+date+"';\n" +
                "\tIF\n" +
                "\t\t@@ERROR <> 0 BEGIN\n" +
                "\t\t\tROLLBACK TRAN;\n" +
                "\t\tRETURN;\n" +
                "\t\t\n" +
                "\tEND SELECT\n" +
                "\tscope_identity() line_id;\n" +
                "COMMIT TRAN;";
    }

    public static String getCurrentCount() {
        return "select\n" +
                "\tl.id line_id,\n" +
                "\tl.bar_code + ': ' + p.name product_name,\n" +
                "\tN'Тоо.х: ' + cast(round(l.qty, 4) as varchar(20)) + N'   Үнийн дүн: ' + cast(round(l.qty * isnull(l.sale_price, pp.sale_price), 2) as varchar(20)) qty_and_price\n" +
                "from\n" +
                "\tsto_counting c with (nolock) left join\n" +
                "\tsto_counting_line l with (nolock) on c.full_id = l.full_id left join\n" +
                "\tsto_product p with (nolock) on l.group_num = p.group_num left join\n" +
                "\tsto_product_price pp with (nolock) on\n" +
                "\t\tl.group_num = pp.group_num and\n" +
                "\t\tpp.deleteddate is null and\n" +
                "\t\tc.warehouse_id = pp.warehouse_id\n" +
                "where\n" +
                "\tc.full_id = ? and\n" +
                "\tl.systemdate = ?\n" +
                "order by l.createddate desc;";
    }

    public static String getOneToollogo() {
        return "SELECT\n" +
                "\tl.id line_id,\n" +
                "\tl.bar_code + ': ' + p.name product_name,\n" +
                "\tN'Тоо.х: ' + CAST (\n" +
                "\t\tround( l.qty, 4 ) AS VARCHAR ( 20 )) + N'   Үнийн дүн: ' + CAST (\n" +
                "\tround( l.qty * isnull( l.sale_price, pp.sale_price ), 2 ) AS VARCHAR ( 20 )) qty_and_price \n" +
                "FROM\n" +
                "\tsto_counting c WITH ( nolock )\n" +
                "\tLEFT JOIN sto_counting_line l WITH ( nolock ) ON c.full_id = l.full_id\n" +
                "\tLEFT JOIN sto_product p WITH ( nolock ) ON l.group_num = p.group_num\n" +
                "\tLEFT JOIN sto_product_price pp WITH ( nolock ) ON l.group_num = pp.group_num \n" +
                "\tAND pp.deleteddate IS NULL \n" +
                "\tAND c.warehouse_id = pp.warehouse_id \n" +
                "WHERE\n" +
                "\tc.full_id = ? \n" +
                "ORDER BY\n" +
                "\tl.createddate DESC;";
    }

    public static String deleteLine() {
        return "delete from sto_counting_line where id = ?;";
    }

    public static String getNameCount() {
        return "select c.full_id + ', ' + convert(varchar(20), c.[date], 111) + ': ' + w.name + ', ' + cast(count(l.id) as varchar(10)) + N'ш' toollogo_name\n" +
                "from\n" +
                "\tsto_counting c with (nolock) left join\n" +
                "\tsto_counting_line l with (nolock) on c.full_id = l.full_id inner join\n" +
                "\tsto_warehouse w with (nolock) on c.warehouse_id = w.id\n" +
                "where c.full_id = ?\n" +
                "group by c.full_id, c.[date], w.name;";
    }

    public static String setToken() {
        return "update tUser set FCM_token = null where FCM_token = ?; " +
                "update tUser set FCM_token = ? where UserID = (select login_user_id from staff with (nolock) where id = ?)";
    }


}
