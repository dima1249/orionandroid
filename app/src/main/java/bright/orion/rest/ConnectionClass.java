package bright.orion.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import bright.orion.app.Const;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by orgil on 12/10/2017.
 */

public class ConnectionClass {

    public Connection CONN(Context context) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL;

        try {
            String classs = "net.sourceforge.jtds.jdbc.Driver";
            Class.forName(classs);

            SharedPreferences shared = context.getSharedPreferences(Const.PREF_FILE_NAME, MODE_PRIVATE);
            String ip = shared.getString(Const.IP, "103.17.108.24");
            String db = shared.getString(Const.DB, "op_test2");
            String user = shared.getString(Const.USER, "op_test_user2");
            String pass = shared.getString(Const.DBPASSWORD, "orion123");

            ConnURL = "jdbc:jtds:sqlserver://" + ip + ";"
                    + "databaseName=" + db + ";user=" + user + ";password="
                    + pass + ";";
            conn = DriverManager.getConnection(ConnURL);
        } catch (SQLException se) {
            Log.e("ERRO", se.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e("ERRO", e.getMessage());
        } catch (Exception e) {
            Log.e("ERRO", e.getMessage());
        }
        return conn;
    }
}
