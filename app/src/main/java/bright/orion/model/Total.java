package bright.orion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by orgil on 12/12/2017.
 */

public class Total implements Serializable {

    private int totalOrderLPG = 0;
    private int totalBallon = 0;
    private List<Ballon> ballonCountList = new ArrayList<>();
    private int totalPrice = 0;
    private List<Ballon> ballonPriceList = new ArrayList<>();


    public Total() {
    }

    public int getTotalOrderLPG() {
        return totalOrderLPG;
    }

    public void setTotalOrderLPG(int totalOrderLPG) {
        this.totalOrderLPG = totalOrderLPG;
    }

    public int getTotalBallon() {
        return totalBallon;
    }

    public void setTotalBallon(int totalBallon) {
        this.totalBallon = totalBallon;
    }

    public List<Ballon> getBallonCountList() {
        return ballonCountList;
    }

    public void setBallonCountList(List<Ballon> ballonCountList) {
        this.ballonCountList = ballonCountList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Ballon> getBallonPriceList() {
        return ballonPriceList;
    }

    public void setBallonPriceList(List<Ballon> ballonPriceList) {
        this.ballonPriceList = ballonPriceList;
    }

    public int getTotalComplain() {
        return totalComplain;
    }

    public void setTotalComplain(int totalComplain) {
        this.totalComplain = totalComplain;
    }

    private int totalComplain;


}
