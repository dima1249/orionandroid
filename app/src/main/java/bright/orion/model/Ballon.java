package bright.orion.model;

import java.io.Serializable;

/**
 * Created by orgil on 12/12/2017.
 */

public class Ballon implements Serializable {

    private int id = -1;
    private int soldId = -1;
    private String type;
    private String info;
    private String number = "";
    private String year = "Он";
    private float qty;

    public Ballon() {
    }

    public Ballon(String type, String number) {
        this.type = type;
        this.number = number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static class Builder {
        private int id = -1;
        private int soldId;
        private String type;
        private String info;
        private String number = "";
        private String year = "Он";
        private float qty;

        public Builder() {
        }

        public Builder id(int id) {
            if (id > 0)
                this.id = id;
            else
                this.id = -1;
            return this;
        }


        public Builder soldId(int soldId) {
            if (soldId > 0)
                this.soldId = soldId;
            else
                this.soldId = -1;
            return this;
        }

        public Builder info(String info) {
            this.info = "";
            if (info != null)
                this.info = info;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder number(String number) {
            if (number != null)
                this.number = number;
            else {
                this.number = "";
            }
            return this;
        }

        public Builder year(String year) {
            if (year != null)
                this.year = year;
            else {
                this.year = "Он";
            }
            return this;
        }

        public Builder qty(float qty) {
            this.qty = qty;
            return this;
        }

        public Ballon build() {
            Ballon ballon = new Ballon();
            ballon.setId(id);
            ballon.setSoldId(soldId);
            ballon.setType(type);
            ballon.setInfo(info);
            ballon.setNumber(number);
            ballon.setYear(year);
            ballon.setQty(qty);
            return ballon;
        }

    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSoldId() {
        return soldId;
    }

    public void setSoldId(int soldId) {
        this.soldId = soldId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public float getQty() {
        return qty;
    }

    public void setQty(float qty) {
        this.qty = qty;
    }

}
