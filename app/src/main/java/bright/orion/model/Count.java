package bright.orion.model;

import java.io.Serializable;

/**
 * Created by orgil on 12/12/2017.
 */

public class Count implements Serializable {

    private int id;
    private String fullId;
    private String name;
    private String state;
    private String totalPrice;

    public Count(int id, String fullId, String name, String state, String totalPrice) {
        this.id = id;
        this.fullId = fullId;
        this.name = name;
        this.state = state;
        this.totalPrice = totalPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullId() {
        return fullId;
    }

    public void setFullId(String fullId) {
        this.fullId = fullId;
    }
}
