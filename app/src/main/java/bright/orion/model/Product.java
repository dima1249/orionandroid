package bright.orion.model;

import java.io.Serializable;

/**
 * Created by orgil on 12/12/2017.
 */

public class Product implements Serializable {


    private String barCode;
    private int groupNum;
    private String name = "";
    private String category = "";
    private double price;


    public Product(String barCode, int groupNum, String name, String category, double price) {
        this.barCode = barCode;
        this.groupNum = groupNum;
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public Product() {
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public int getGroupNum() {
        return groupNum;
    }

    public void setGroupNum(int groupNum) {
        this.groupNum = groupNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
