package bright.orion.model;

import java.io.Serializable;

/**
 * Created by orgil on 12/12/2017.
 */

public class Order implements Serializable {

    private int id;
    private String partnerNamePhone;
    private String address;
    private String ballon;
    private String date = "";
    private int isNew;
    private String createdDate = "";
    private String doneDate = "";
    private String note = "";
    private boolean hasComplain;

    public Order() {

    }

    public Order(int id, String partnerNamePhone, String address, String ballon, String date,
                 int isNew) {
        this.id = id;
        this.partnerNamePhone = partnerNamePhone;
        this.address = address;
        this.ballon = ballon;
        this.date = date;
        this.isNew = isNew;
    }

    public static class Builder {
        private String createdDate;
        private String doneDate;
        private String note;
        private boolean hasComplain;

        public Builder() {
        }

        public Builder createdDate(String createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder doneDate(String doneDate) {
            this.doneDate = doneDate;
            return this;
        }

        public Builder note(String note) {
            this.note = note;
            return this;
        }

        public Builder hasComplain(boolean hasComplain) {
            this.hasComplain = hasComplain;
            return this;
        }

        public Order build() {
            Order order = new Order();
            order.setCreatedDate(createdDate);
            order.setDoneDate(doneDate);
            order.setNote(note);
            order.setHasComplain(hasComplain);
            return order;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartnerNamePhone() {
        return partnerNamePhone;
    }

    public void setPartnerNamePhone(String partnerNamePhone) {
        this.partnerNamePhone = partnerNamePhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBallon() {
        return ballon;
    }

    public void setBallon(String ballon) {
        this.ballon = ballon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getIsNew() {
        return isNew;
    }

    public void setIsNew(int isNew) {
        this.isNew = isNew;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDoneDate() {
        return doneDate;
    }

    public void setDoneDate(String doneDate) {
        if (doneDate != null)
            this.doneDate = doneDate;
        else {
            this.doneDate = "";
        }
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean getHasComplain() {
        return hasComplain;
    }

    public void setHasComplain(boolean hasComplain) {
        this.hasComplain = hasComplain;
    }

}
