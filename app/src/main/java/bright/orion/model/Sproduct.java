package bright.orion.model;

import java.io.Serializable;

/**
 * Created by orgil on 12/12/2017.
 */

public class Sproduct implements Serializable {

    private int lineId;
    private String name = "";
    private String description = "";
    private boolean isChecked = false;


    public Sproduct(int lineId, String name, String description) {
        this.lineId = lineId;
        this.name = name;
        this.description = description;
    }

    public Sproduct() {
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
