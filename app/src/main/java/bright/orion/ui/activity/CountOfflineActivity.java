package bright.orion.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bright.orion.R;
import bright.orion.model.Count;
import bright.orion.model.Sproduct;
import bright.orion.ui.adapter.SproductAdapter;
import bright.orion.ui.dialog.ConfirmDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.schaumburgit.fastbarcodescanner.BarcodeScannerFactory;
import dk.schaumburgit.fastbarcodescanner.IBarcodeScanner;

public class CountOfflineActivity extends AppCompatActivity implements SproductAdapter.ItemClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    ConstraintLayout mContainer;
    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.scan_btn)
    ImageButton btnScan;
    @BindView(R.id.bar_code)
    EditText barCode;
    @BindView(R.id.product_qty)
    EditText productQty;
    @BindView(R.id.save_btn)
    Button btnSave;
    @BindView(R.id.recyclerSold)
    RecyclerView mRecycler;
    @BindView(R.id.delete_btn)
    Button btnDelete;

    IBarcodeScanner mScanner = null;
    private static final int REQUEST_BAR_CODE = 2;
    private static final int REQUEST_COUNTS_CODE = 3;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private List<Sproduct> sProductList = new ArrayList<>();
    Count count;
    SproductAdapter mAdapter;
    boolean isChanged = false;
    private String fileName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_offline);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        showProgress(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChanged) {
                    final ConfirmDialog dg = new ConfirmDialog(CountOfflineActivity.this,
                            "Та хадгалахгүйгээр гарах гэж байна Үнэхээр гарах уу?");
                    dg.show();
                    Button btnYes = dg.getYesButton();
                    if (btnYes != null)
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dg.dismiss();
                                onBackPressed();
                            }
                        });
                } else onBackPressed();
            }
        });
        if (Build.VERSION.SDK_INT >= 23) {
            this.requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION
            );
        }

        mAdapter = new SproductAdapter(sProductList, this);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);

        count = (Count) getIntent().getSerializableExtra("count");
        fileName = count.getName();

        mScanner = BarcodeScannerFactory.builder().build(this);
        getsProductList();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @OnClick(R.id.scan_btn)
    public void onClickScanBtn() {
        Intent intent = new Intent(getApplicationContext(), BarCodeScanActivity.class);
        startActivityForResult(intent, REQUEST_BAR_CODE);
    }

    @OnClick(R.id.save_btn)
    public void onClickSaveBtn() {
        if (barCode.getText().toString().length() > 0)
            if (Double.valueOf(productQty.getText().toString()) > 0) {
                showProgress(true);
                String data = barCode.getText().toString() + ":" + Double.valueOf(productQty.getText().toString()) + "|";
                writeToFile(data,true);
                sProductList.add(new Sproduct(0, barCode.getText().toString().trim(), productQty.getText().toString()));
                mAdapter.notifyDataSetChanged();
                //readFromFile();
                productQty.setText("");
                productQty.clearFocus();
                isChanged = false;
                showProgress(false);
            } else {
                Toast.makeText(getApplicationContext(), "Тоо хэмжээгээ оруулна уу!", Toast.LENGTH_LONG).show();
            }
        else
            Toast.makeText(getApplicationContext(), "Бар код оруулна уу!", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.delete_btn)
    public void onClickDelete() {
        for (int i = 0; i < sProductList.size(); i++) {
            if (sProductList.get(i).isChecked()) {
                showProgress(true);
                sProductList.remove(i);
            }
        }
        String data = "";
        for (int i = 0; i < sProductList.size(); i++) {
            data += sProductList.get(i).getName() + ":" + sProductList.get(i).getDescription() + "|";
        }
        Log.e("data--->", data);
        writeToFile(data, false);
        getsProductList();
        showProgress(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        switch (requestCode) {
            case REQUEST_BAR_CODE:
                if (RESULT_OK == resultCode) {
                    setBarCode(intent);
                    break;
                } else finish();
            case REQUEST_COUNTS_CODE:
                if (RESULT_OK == resultCode) {
                    getsProductList();
                    break;
                } else finish();
            default:
                break;
        }

    }


    private void setBarCode(Intent intent) {
        String strBarCode = intent.getStringExtra("barCode");
        barCode.setText(strBarCode);
        productQty.requestFocus();
        isChanged = true;
    }

    @Override
    public void onClickOrder(int position) {

    }

    private void getsProductList() {

        String allList = readFromFile();
        Log.e("allList--->", allList);

        // analyzing the string
        String[] tokensVal = allList.split("\\|");

        // prints the count of tokens
        System.out.println("Count of tokens = " + tokensVal.length);

        sProductList.clear();
        int i = 0;
        for (int j = 0; j < tokensVal.length - 1; j++) {
//        for (String token : tokensVal) {
            String[] subToken = tokensVal[j].split("\\:");
            sProductList.add(new Sproduct(i, subToken[0], subToken[1]));
            System.out.print(subToken[0] + "---" + subToken[1]+" ");
            i++;
//        }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void deleteLine(int lineId) {

    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mContainer.setVisibility(show ? View.GONE : View.VISIBLE);
        mContainer.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mContainer.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgress.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void writeToFile(String data, boolean append) {
        try {
            //data = "111:11|222:22|333:33|444:44";
            String rootPath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/Download/";
            File root = new File(rootPath + fileName);
            FileOutputStream stream = new FileOutputStream(root, append);
            stream.write(data.getBytes());
            stream.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile() {
        String rootPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/Download/";
        String ret = "";
        File file = new File(rootPath, fileName);

        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            return text.toString();
        } catch (IOException e) {
            //You'll need to add proper error handling here
        }

        return ret;
    }

}
