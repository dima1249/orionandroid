package bright.orion.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bright.orion.App;
import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.model.User;
import bright.orion.rest.Queries;
import bright.orion.ui.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


    private static final String TAG = "LoginActivity-->";
    private UserLoginTask mAuthTask = null;
    public SharedPreferences shared;

    // UI references.
    @BindView(R.id.viewGroup)
    LinearLayout mViewGroup;
    @BindView(R.id.email)
    AutoCompleteTextView mEmailView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.login_progress)
    View mProgressView;
    @BindView(R.id.login_form)
    View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        shared = getSharedPreferences(Const.PREF_FILE_NAME, MODE_PRIVATE);

        // Set up the login form.
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        try {
            String name = shared.getString(Const.USER_NAME, "");
            String pass = shared.getString(Const.PASSWORD, "");

            mEmailView.setText(URLDecoder.decode(name, "UTF-8"));
            mPasswordView.setText(URLDecoder.decode(pass, "UTF-8"));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (!Utils.isNetworkAvailable(LoginActivity.this)) {
            Toast.makeText(getApplicationContext(),
                    R.string.error_no_connection,
                    Toast.LENGTH_SHORT).show();
            showProgress(false);
        } else {
            new CountDownTimer(800, 800) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    if (mEmailView.length() > 0 && mPasswordView.length() > 0) {
                        attemptLogin();
                    } else {
                        showProgress(false);
                    }
                }
            }.start();
        }

    }

    @OnClick(R.id.count_off_btn)
    public void onClickCountOff(View v) {
        Intent intent = new Intent(this, MainFileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.settings_btn)
    public void onClickSettingsBtn(View v) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.email_sign_in_button)
    public void onClickSignButton(View v) {
        attemptLogin();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_pass_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_username_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            //Check internet connection
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(getApplicationContext(), R.string.error_no_connection,
                        Toast.LENGTH_SHORT).show();
                showProgress(false);
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);
                mAuthTask = new UserLoginTask(email, password);
                mAuthTask.execute((Void) null);
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    /**
     * Represents an asynchronous login task used to authenticate
     * the user.
     */
    @SuppressLint("StaticFieldLeak")
    public class UserLoginTask extends AsyncTask<Void, Void, Intent> {

        private final String mUserName;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mUserName = email;
            mPassword = password;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
                App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.login(mUserName, mPassword);
                    PreparedStatement preStmt = con.prepareStatement(query);
//                    Log.e("error---->", preStmt.toString());
                    ResultSet rs = preStmt.executeQuery();

                    if (rs.next()) {
                        User user = new User(rs.getInt("UserId"), rs.getString("user_name"));
                        data.putSerializable(Const.ARG_LOGIN, user);
                        res.putExtras(data);
                        return res;
                    } else {
                        Log.e("error---->", rs.toString());
                        data.putString(Const.KEY_ERROR_MESSAGE, "Invalid Credentials");
                        res.putExtras(data);
                        return res;
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mAuthTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                showProgress(false);
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    if (bundle.getString(Const.KEY_ERROR_MESSAGE, "").contains(
                            "Invalid Credentials")) {
                        mPasswordView.setError(getString(R.string.error_incorrect_password));
                        mPasswordView.requestFocus();
                    } else
                        Toast.makeText(getBaseContext(),
                                res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                                Toast.LENGTH_LONG).show();
            } else {
                shared.edit().putString(Const.USER_NAME, Utils.encodeUTF(mUserName)).apply();
                shared.edit().putString(Const.PASSWORD, Utils.encodeUTF(mPassword)).apply();

                setResult(RESULT_OK, res);
                finish();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

