package bright.orion.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import bright.orion.App;
import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.model.Count;
import bright.orion.model.Product;
import bright.orion.model.Sproduct;
import bright.orion.model.User;
import bright.orion.rest.Queries;
import bright.orion.ui.adapter.SproductAdapter;
import bright.orion.ui.dialog.ConfirmDialog;
import bright.orion.ui.dialog.SearchDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.schaumburgit.fastbarcodescanner.BarcodeScannerFactory;
import dk.schaumburgit.fastbarcodescanner.IBarcodeScanner;

public class CountOnlineActivity extends AppCompatActivity implements SproductAdapter.ItemClickListener, SearchDialog.ProductClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    ConstraintLayout mContainer;
    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.scan_btn)
    ImageButton btnScan;
    @BindView(R.id.bar_code)
    EditText barCode;
    @BindView(R.id.set_btn)
    ImageButton btnSet;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.search_btn)
    ImageButton btnSearch;
    @BindView(R.id.product_price)
    TextView productPrice;
    @BindView(R.id.product_category)
    TextView productCategory;
    @BindView(R.id.product_qty)
    EditText productQty;
    @BindView(R.id.save_btn)
    Button btnSave;
    @BindView(R.id.recyclerSold)
    RecyclerView mRecycler;
    @BindView(R.id.delete_btn)
    Button btnDelete;
    @BindView(R.id.see_btn)
    Button btnSee;

    IBarcodeScanner mScanner = null;
    private GetProducts mAuthTask = null;
    private SaveCount mSaveTask = null;
    private GetCurrentCount mCountTask = null;
    private DeleteLine mDeleteTask = null;
    private static final int REQUEST_BAR_CODE = 2;
    private static final int REQUEST_COUNTS_CODE = 3;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private List<Product> productList = new ArrayList<>();
    private List<Sproduct> sProductList = new ArrayList<>();
    private boolean isLoading = true;

    DateTime activityTime;
    Product product = null;
    Count count;
    User user;
    SproductAdapter mAdapter;
    boolean isChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_online);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        showProgress(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChanged) {
                    final ConfirmDialog dg = new ConfirmDialog(CountOnlineActivity.this,
                            "Та хадгалахгүйгээр гарах гэж байна Үнэхээр гарах уу?");
                    dg.show();
                    Button btnYes = dg.getYesButton();
                    if (btnYes != null)
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dg.dismiss();
                                onBackPressed();
                            }
                        });
                } else onBackPressed();
            }
        });

        if (Build.VERSION.SDK_INT >= 23) {
            this.requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION
            );
        }

        mAdapter = new SproductAdapter(sProductList, this);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);

        count = (Count) getIntent().getSerializableExtra("count");
        Log.wtf("Full ID", count.getFullId());
        //
        user = (User) getIntent().getSerializableExtra("user");
        toolbar.setTitle(user.getName());
        mScanner = BarcodeScannerFactory.builder().build(this);
        mAuthTask = new GetProducts(count.getFullId());
        mAuthTask.execute((Void) null);
//        mCountTask = new GetCurrentCount(count.getFullId(), activityTime.getMillis());
//        mCountTask.execute((Void) null);

        //time

        Calendar calendar = Calendar.getInstance();
        TimeZone tz = calendar.getTimeZone();
        DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
        activityTime = new DateTime(calendar.getTimeInMillis(), jodaTz);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @OnClick(R.id.scan_btn)
    public void onClickScanBtn() {
        Intent intent = new Intent(getApplicationContext(), BarCodeScanActivity.class);
        startActivityForResult(intent, REQUEST_BAR_CODE);
    }

    @OnClick(R.id.set_btn)
    public void onClickSetBtn() {
        product = searchProduct(barCode.getText().toString());
        if (product != null) {
            productName.setText(product.getName());
            productPrice.setText(String.valueOf(product.getPrice()));
            productCategory.setText(product.getCategory());
            productQty.requestFocus();
            btnSave.setEnabled(true);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Хайсан бараа байхгүй байна",
                    Toast.LENGTH_LONG).show();
            btnSave.setEnabled(false);
            productName.setText("");
            productPrice.setText("");
            productCategory.setText("");
            productQty.setText(null);
        }
    }

    @OnClick(R.id.save_btn)
    public void onClickSaveBtn() {
        if (product != null) {
//            try {

            if (!productQty.getText().toString().equals("") && Double.valueOf(productQty.getText().toString()) > 0) {
                showProgress(true);
                mSaveTask = new SaveCount(product.getBarCode(), Float.valueOf(productQty.getText().toString()), count.getFullId(),
                        activityTime.getMillis(), user.getId(), product.getGroupNum(), product.getPrice(), product.getName());
                mSaveTask.execute((Void) null);
                productQty.setText("");
                productQty.clearFocus();
                isChanged = false;
            }
            else{
                productQty.setError("Хоосон байна.");
                productQty.requestFocus();
            }
//            } catch (ParseException e) {
//                productQty.setError("Тоо оруулна уу.");
//                productQty.requestFocus();
//            }
        }
    }

    @OnClick(R.id.delete_btn)
    public void onClickDelete() {
        for (int i = 0; i < sProductList.size(); i++) {
            if (sProductList.get(i).isChecked()) {
                showProgress(true);
                mDeleteTask = new DeleteLine(sProductList.get(i).getLineId());
                mDeleteTask.execute((Void) null);
                mCountTask = new GetCurrentCount(count.getFullId(), activityTime.getMillis());
                mCountTask.execute((Void) null);
            }
        }
    }

    @OnClick(R.id.see_btn)
    public void onClickSee() {
        Intent intent = new Intent(getApplicationContext(), AllProductList.class);
        intent.putExtra("count", count);
        intent.putExtra("user", user);
        startActivityForResult(intent, REQUEST_COUNTS_CODE);
    }

    @OnClick(R.id.search_btn)
    public void onClickSearch() {
        final SearchDialog dg = new SearchDialog(CountOnlineActivity.this,
                productList, this);
        dg.show();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        switch (requestCode) {
            case REQUEST_BAR_CODE:
                if (RESULT_OK == resultCode) {
                    setBarCode(intent);
                    break;
                } else finish();
            case REQUEST_COUNTS_CODE:
                if (RESULT_OK == resultCode) {
                    mCountTask = new GetCurrentCount(count.getFullId(), activityTime.getMillis());
                    mCountTask.execute((Void) null);
                    break;
                } else finish();
            default:
                break;
        }

    }

    private Product searchProduct(String strBarCode) {
        for (int i = 0; i < productList.size(); i++) {
            if (strBarCode.equals(productList.get(i).getBarCode())) {
                return productList.get(i);
            }
        }
        return null;
    }

    private void setBarCode(Intent intent) {
        String strBarCode = intent.getStringExtra("barCode");
        barCode.setText(strBarCode);
        onClickSetBtn();
        isChanged = true;
    }

    @Override
    public void onClickOrder(int position) {

    }

    @Override
    public void onClickProduct(Product product) {
        Log.e("-----Product---->", product.getName());
        String strBarCode = product.getBarCode();
        barCode.setText(strBarCode);
        onClickSetBtn();
        isChanged = true;
    }

    @SuppressLint("StaticFieldLeak")
    public class GetProducts extends AsyncTask<Void, Void, Intent> {

        private final String mFullId;

        GetProducts(String fullId) {
            this.mFullId = fullId;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.getProductByBarCode();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setString(1, mFullId);
                    ResultSet rs = preStmt.executeQuery();
                    productList.clear();
                    ResultSetMetaData metadata = rs.getMetaData();
                    int columnCount = metadata.getColumnCount();
                    while (rs.next()) {
                        productList.add(new Product(rs.getString("bar_code"), rs.getInt("group_num"),
                                rs.getString("name"), rs.getString("category"), rs.getDouble("sale_price")));
//                        for (int i = 1; i <= columnCount; i++) {
//                            if (i > 1) Log.e(" ", ", ");
//                            String columnValue = rs.getString(i);
//                            Log.e(columnValue, " " + metadata.getColumnName(i));
//                        }
                    }
                    Log.e("HAHAHAHA--->", String.valueOf(productList.size()));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mAuthTask = null;
            showProgress(false);
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class SaveCount extends AsyncTask<Void, Void, Intent> {

        private final String barCode;
        private final float qty;
        private final String fullId;
        private final long date;
        private final int userId;
        private final int groupNum;
        private final double price;
        private final String name;

        SaveCount(String barCode, float qty, String fullId, long date, int userId, int groupNum, double price, String name) {
            this.barCode = barCode;
            this.qty = qty;
            this.fullId = fullId;
            this.date = date;
            this.userId = userId;
            this.groupNum = groupNum;
            this.price = price;
            this.name = name;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.saveCount(fullId, barCode, ""+qty,""+this.groupNum,""+this.price,""+this.userId, DateFormat.format("yyyy-MM-dd hh:mm:ss", date).toString());

                    Log.e("save query", query);
                    PreparedStatement preStmt = con.prepareStatement(query);
                    ResultSet rs = preStmt.executeQuery();
                    while (rs.next()) {
                        Integer line_id = rs.getInt("line_id");
                        Log.e("res query", ""+line_id);
                        data.putInt(Const.SUCCESS_RES, line_id);
                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mSaveTask = null;
            showProgress(false);
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                sProductList.add(new Sproduct(res.getExtras().getInt("line_id"), this.name, ""+this.qty+"  Үнийн дүн: "+this.price));
                mAdapter.notifyDataSetChanged();

            }
        }

        @Override
        protected void onCancelled() {
            mSaveTask = null;
        }


    }

    @SuppressLint("StaticFieldLeak")
    public class GetCurrentCount extends AsyncTask<Void, Void, Intent> {

        private final String mFullId;
        private final long date;

        GetCurrentCount(String fullId, long date) {
            this.mFullId = fullId;
            this.date = date;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.getCurrentCount();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setString(1, mFullId);
                    preStmt.setTimestamp(2, new Timestamp(date));
                    ResultSet rs = preStmt.executeQuery();
                    sProductList.clear();
                    ResultSetMetaData metadata = rs.getMetaData();
                    int columnCount = metadata.getColumnCount();
                    while (rs.next()) {
                        sProductList.add(new Sproduct(rs.getInt("line_id"), rs.getString("product_name"), rs.getString("qty_and_price")));
                        String row = "";
                        for (int i = 1; i <= columnCount; i++) {
                            if (i > 1) Log.e(" ", ", ");
                            String columnValue = rs.getString(i);
                            Log.e(columnValue, " " + metadata.getColumnName(i));
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                showProgress(false);
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                showProgress(false);
                mAdapter.notifyDataSetChanged();
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class DeleteLine extends AsyncTask<Void, Void, Intent> {

        private final int mLineId;

        DeleteLine(int lineId) {
            this.mLineId = lineId;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.deleteLine();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setInt(1, mLineId);
                    preStmt.executeUpdate();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            showProgress(false);
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                for (int i = 0; i<sProductList.size(); i++){
                    if (sProductList.get(i).getLineId() == mLineId){
                        sProductList.remove(i);
                        mAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mContainer.setVisibility(show ? View.GONE : View.VISIBLE);
        mContainer.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mContainer.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgress.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
