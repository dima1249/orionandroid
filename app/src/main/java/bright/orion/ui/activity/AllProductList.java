package bright.orion.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import bright.orion.App;
import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.model.Count;
import bright.orion.model.Sproduct;
import bright.orion.model.User;
import bright.orion.rest.Queries;
import bright.orion.ui.adapter.SproductAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AllProductList extends AppCompatActivity implements SproductAdapter.ItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.count_name)
    TextView countName;
    @BindView(R.id.recycler_view)
    RecyclerView mRecycler;

    private List<Sproduct> sProductList = new ArrayList<>();
    SproductAdapter mAdapter;
    private GetCurrentCount mCountTask = null;
    private DeleteLine mDeleteTask = null;
    private GetNameCount mNameTask = null;
    private String mCounName = "";
    Count count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_product_list);
        ButterKnife.bind(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mAdapter = new SproductAdapter(sProductList, this);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);
        count = (Count) getIntent().getSerializableExtra("count");
        User user = (User) getIntent().getSerializableExtra("user");
        toolbar.setTitle(user.getName());
        mNameTask = new GetNameCount(count.getFullId());
        mNameTask.execute();
        mCountTask = new GetCurrentCount(count.getFullId());
        mCountTask.execute((Void) null);
        countName.setText(mCounName);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    @OnClick(R.id.delete_btn)
    public void onClickDelete() {
        for (int i = 0; i < sProductList.size(); i++) {
            if (sProductList.get(i).isChecked()) {
                mDeleteTask = new DeleteLine(sProductList.get(i).getLineId());
                mDeleteTask.execute((Void) null);
            }
        }
    }

    @Override
    public void onClickOrder(int position) {

    }


    @SuppressLint("StaticFieldLeak")
    public class GetCurrentCount extends AsyncTask<Void, Void, Intent> {

        private final String mFullId;

        GetCurrentCount(String fullId) {
            this.mFullId = fullId;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
//                res.putExtras(data);
//                return res;
//            }

            try {
                App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.getOneToollogo();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setString(1, mFullId);
                    ResultSet rs = preStmt.executeQuery();
                    sProductList.clear();
                    ResultSetMetaData metadata = rs.getMetaData();
                    int columnCount = metadata.getColumnCount();
                    while (rs.next()) {

                        if(rs.getString("product_name") == null)
                            break;

                        sProductList.add(new Sproduct(rs.getInt("line_id"), rs.getString("product_name"), rs.getString("qty_and_price")));
                        String row = "";
                        for (int i = 1; i <= columnCount; i++) {
                            if (i > 1) Log.e(" ", ", ");
                            String columnValue = rs.getString(i);
                            Log.e(columnValue, " " + metadata.getColumnName(i));
                        }
                    }
                    Log.e("HAHAHAHA--->", String.valueOf(sProductList.size()));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                mAdapter.notifyDataSetChanged();
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class DeleteLine extends AsyncTask<Void, Void, Intent> {

        private final int mLineId;

        DeleteLine(int lineId) {
            this.mLineId = lineId;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
//                res.putExtras(data);
//                return res;
//            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.deleteLine();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setInt(1, mLineId);
                    preStmt.executeUpdate();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                for (int i = 0; i<sProductList.size(); i++){
                    if (sProductList.get(i).getLineId() == mLineId){
                        sProductList.remove(i);
                        mAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class GetNameCount extends AsyncTask<Void, Void, Intent> {

        private final String mFullId;

        GetNameCount(String fullId) {
            this.mFullId = fullId;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
//            try {
//                // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
//                res.putExtras(data);
//                return res;
//            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.getNameCount();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setString(1, mFullId);
                    ResultSet rs = preStmt.executeQuery();
                    while (rs.next()) {
                        data.putString(Const.SUCCESS_RES, rs.getString("toollogo_name"));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                countName.setText(res.getExtras().getString(Const.SUCCESS_RES));
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }


}
