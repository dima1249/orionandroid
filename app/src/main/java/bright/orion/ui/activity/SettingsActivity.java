package bright.orion.ui.activity;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;

import bright.orion.App;
import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.ui.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.connect_ip)
    AutoCompleteTextView connecIp;
    @BindView(R.id.db_name)
    AutoCompleteTextView dbName;
    @BindView(R.id.user_name)
    AutoCompleteTextView userName;
    @BindView(R.id.password)
    AutoCompleteTextView password;

    private SharedPreferences shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        shared = getSharedPreferences(Const.PREF_FILE_NAME, MODE_PRIVATE);
        try {
            String ip = shared.getString(Const.IP, "103.17.108.24");
            String db = shared.getString(Const.DB, "op_test2");
            String user = shared.getString(Const.USER, "op_test_user2");
            String pass = shared.getString(Const.DBPASSWORD, "orion123");

//            String ip = "";
//            String db = "";
//            String user = "";
//            String pass = "";

            connecIp.setText(URLDecoder.decode(shared.getString(Const.IP, ip), "UTF-8"));
            dbName.setText(URLDecoder.decode(shared.getString(Const.DB, db), "UTF-8"));
            userName.setText(URLDecoder.decode(shared.getString(Const.USER, user), "UTF-8"));
            password.setText(URLDecoder.decode(shared.getString(Const.DBPASSWORD, pass), "UTF-8"));

//            String ip = shared.getString(Const.IP, "103.17.108.24");
//            String db = shared.getString(Const.DB, "op_test2");
//            String user = shared.getString(Const.USER, "op_test_user2");
//            String pass = shared.getString(Const.DBPASSWORD, "orion123");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @OnClick(R.id.save_btn)
    public void onClickSave(View v) {
        // Check for a valid password, if the user entered one.
        // Store values at the time of the login attempt.
        String ip = connecIp.getText().toString();
        String db = dbName.getText().toString();
        String user = userName.getText().toString();
        String pass = password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(pass)) {
            password.setError(getString(R.string.error_pass_field_required));
            focusView = password;
            cancel = true;
        }
        if (TextUtils.isEmpty(user)) {
            userName.setError(getString(R.string.error_username_field_required));
            focusView = userName;
            cancel = true;
        }
        if (TextUtils.isEmpty(db)) {
            dbName.setError(getString(R.string.error_username_field_required));
            focusView = dbName;
            cancel = true;
        }
        if (TextUtils.isEmpty(ip)) {
            connecIp.setError(getString(R.string.error_username_field_required));
            focusView = connecIp;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            //Check internet connection
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(getApplicationContext(), R.string.error_no_connection,
                        Toast.LENGTH_SHORT).show();
            } else {
                shared.edit().putString(Const.IP, ip).apply();
                shared.edit().putString(Const.DB, db).apply();
                shared.edit().putString(Const.USER, user).apply();
                shared.edit().putString(Const.DBPASSWORD, pass).apply();

                App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    Toast.makeText(getApplicationContext(), "Сервертэй холбогдоход алдаа гарлаа шалгаад дахин оролдон уу", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Тохиргоо амжилттай хийгдлээ", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }
    }
}
