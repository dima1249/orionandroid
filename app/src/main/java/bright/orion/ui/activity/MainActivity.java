package bright.orion.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import bright.orion.App;
import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.model.Count;
import bright.orion.model.User;
import bright.orion.rest.Queries;
import bright.orion.ui.adapter.CountsAdapter;
import bright.orion.ui.dialog.ConfirmDialog;
import bright.orion.ui.listener.OnDataPass;
import bright.orion.ui.util.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static org.joda.time.Days.daysBetween;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, OnDataPass, SwipeRefreshLayout.OnRefreshListener, CountsAdapter.ItemClickListener {

    private static final int REQUEST_LOGIN = 0;

    @BindView(R.id.userinfo)
    TextView mUserInfo;
    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.date)
    TextView mDate;

    @BindView(R.id.empty_view)
    TextView tEmpty;

    @BindView(R.id.swiper)
    SwipeRefreshLayout mRefresh;
    @BindView(R.id.recyclerView)
    RecyclerView mRecycler;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private User mUser;
    private DateTime mDateEnd;
    private DateTime mDateStart;
    CountsAdapter mAdapter;
    private GetCounts mAuthTask = null;
    private List<Count> countList = new ArrayList<>();

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_settings:
                    Intent intent = new Intent(getApplicationContext(),
                            SettingsActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_logout:
                    final ConfirmDialog dg = new ConfirmDialog(MainActivity.this,
                            "Та аппликейшн-с гарах гэж байна! Үнэхээр гарах уу?");
                    dg.show();
                    Button btnYes = dg.getYesButton();
                    if (btnYes != null)
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences shared = getSharedPreferences(
                                        Const.PREF_FILE_NAME,
                                        MODE_PRIVATE);
                                shared.edit().remove(Const.USER_NAME).apply();
                                shared.edit().remove(Const.PASSWORD).apply();
                                shared.edit().putBoolean(Const.LOGOUT, true).apply();

                                dg.dismiss();
                                Intent intent = new Intent(getApplicationContext(),
                                        LoginActivity.class);
                                intent.addFlags(
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);
                            }
                        });
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //call Login Activity
        startSignIn();
    }

    @OnClick(R.id.date)
    public void onSelectDate(View view) {
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                MainActivity.this,
                mDateStart.getYear(),
                mDateStart.getMonthOfYear() - 1,
                mDateStart.getDayOfMonth(),
                mDateEnd.getYear(),
                mDateEnd.getMonthOfYear() - 1,
                mDateEnd.getDayOfMonth()
        );
        if (!dpd.isVisible())
            dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    private void startSignIn() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, REQUEST_LOGIN);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        switch (requestCode) {
            case REQUEST_LOGIN:
                if (RESULT_OK == resultCode) {
                    initializeView(intent);
                    break;
                } else finish();
            default:
                break;
        }

    }

    private void initializeView(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            mUser = (User) bundle.getSerializable(Const.ARG_LOGIN);
            assert mUser != null;
            mUserInfo.setText(mUser.getName());
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mRefresh.setOnRefreshListener(this);
        mAdapter = new CountsAdapter(countList, this);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);

        mDateEnd = new DateTime();
        mDateStart = mDateEnd.minusDays(90);
        mDate.setText(
                mDateStart.getMonthOfYear() + "/" + mDateStart.getDayOfMonth() + "-" +
                        mDateEnd.getMonthOfYear() + "/" + mDateEnd.getDayOfMonth());

        getData();
    }

    @SuppressLint("SetTextI18n")
    private void getData() {
        showProgress(true);
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), R.string.error_no_connection,
                    Toast.LENGTH_SHORT).show();
            showProgress(false);
        } else {
            mRefresh.setRefreshing(true);
            mAuthTask = new GetCounts(mDateStart, mDateEnd);
            mAuthTask.execute((Void) null);
        }
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth,
                          int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        TimeZone tz = Calendar.getInstance().getTimeZone();
        DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
        mDateStart = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0, 0, jodaTz);
        mDateEnd = new DateTime(yearEnd, monthOfYearEnd + 1, dayOfMonthEnd, 0, 0, 0, jodaTz);
        int days = daysBetween(mDateStart.toLocalDate(), mDateEnd.toLocalDate()).getDays();

        if (days < 0) {
            Toast.makeText(getApplicationContext(), "Эхлэх өдөр дуусахаасаа өмнө байх ёстой!",
                    Toast.LENGTH_LONG).show();
        } else {
            mDate.setText(String.valueOf(monthOfYear + 1) + "/" + dayOfMonth + "-" + String.valueOf(
                    monthOfYearEnd + 1) + "/" + dayOfMonthEnd);
            getData();
        }

    }

    @SuppressLint("StaticFieldLeak")
    public class GetCounts extends AsyncTask<Void, Void, Intent> {

        private final DateTime mDateStart;
        private final DateTime mDateEnd;

        GetCounts(DateTime dateStart, DateTime dateEnd) {
            this.mDateStart = dateStart;
            this.mDateEnd = dateEnd;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.getCounts();
//                    Log.e("query--->", query);
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setDate(1, new java.sql.Date(mDateStart.getMillis()));
                    preStmt.setDate(2, new java.sql.Date(mDateStart.getMillis()));
                    preStmt.setDate(3, new java.sql.Date(mDateEnd.getMillis()));
                    preStmt.setDate(4, new java.sql.Date(mDateEnd.getMillis()));
                    ResultSet rs = preStmt.executeQuery();
                    countList.clear();

                    ResultSetMetaData metadata = rs.getMetaData();
                    int columnCount = metadata.getColumnCount();
                    while (rs.next()) {
                        countList.add(new Count(1, rs.getString("full_id"), rs.getString("toollogo_name"),
                                rs.getString("state_code"), rs.getString("total_sale_price")));
//                        String row = "";
//                        for (int i = 1; i <= columnCount; i++) {
//                            if (i > 1) Log.e(" ", ", ");
//                            String columnValue = rs.getString(i);
//                            Log.e(columnValue, " " + metadata.getColumnName(i));
//                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mAuthTask = null;
            mRefresh.setRefreshing(false);
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                showProgress(false);
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                showProgress(false);
                mAdapter.notifyDataSetChanged();
                if (countList.size() > 0) {
                    tEmpty.setVisibility(View.GONE);
                    mRecycler.setVisibility(View.VISIBLE);
                } else {
                    tEmpty.setVisibility(View.VISIBLE);
                    mRecycler.setVisibility(View.GONE);
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            mRefresh.setRefreshing(false);
            showProgress(false);
        }
    }


    @Override
    public void onDataPass(String data) {

    }


    @Override
    public void onRefresh() {
        getData();
    }

    @Override
    public void onClickOrder(int position) {
        Intent intent = new Intent(getApplicationContext(), CountOnlineActivity.class);
        intent.putExtra("count", countList.get(position));
        intent.putExtra("user", mUser);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onLongClickOrder(int position) {

    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mRefresh.setVisibility(show ? View.GONE : View.VISIBLE);
        mRefresh.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRefresh.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgress.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
