package bright.orion.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.model.Count;
import bright.orion.model.User;
import bright.orion.ui.adapter.CountsAdapter;
import bright.orion.ui.dialog.ConfirmDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static org.joda.time.Days.daysBetween;

public class MainFileActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, SwipeRefreshLayout.OnRefreshListener, CountsAdapter.ItemClickListener {

    private static final int REQUEST_LOGIN = 0;

    @BindView(R.id.userinfo)
    TextView mUserInfo;
    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.date)
    TextView mDate;
    @BindView(R.id.swiper)
    SwipeRefreshLayout mRefresh;
    @BindView(R.id.recyclerView)
    RecyclerView mRecycler;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private User mUser;
    private DateTime mDateEnd;
    private DateTime mDateStart;
    CountsAdapter mAdapter;
    private List<Count> countList = new ArrayList<>();
    private String rootPath;
    private File root;

    private static final int REQUEST_FILE_PERMISSION = 1;

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_add:
                    createFile();
                    return true;
                case R.id.navigation_back:
                    finish();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_file);
        ButterKnife.bind(this);
        initializeView();
        if (Build.VERSION.SDK_INT >= 23) {
            if(ContextCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                this.requestPermissions(
                        new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        REQUEST_FILE_PERMISSION
                );
            }
        }
    }

    @OnClick(R.id.date)
    public void onSelectDate(View view) {
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                MainFileActivity.this,
                mDateStart.getYear(),
                mDateStart.getMonthOfYear() - 1,
                mDateStart.getDayOfMonth(),
                mDateEnd.getYear(),
                mDateEnd.getMonthOfYear() - 1,
                mDateEnd.getDayOfMonth()
        );
        if (!dpd.isVisible())
            dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_FILE_PERMISSION) {
            if (grantResults.length != 2 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,
                        "Файлруу хандах зөвшөөрөгдсөнгүй.",
                        Toast.LENGTH_LONG).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void createFile() {
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            this.requestPermissions(
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    REQUEST_FILE_PERMISSION
            );
            return;
        }
        Calendar calendar = Calendar.getInstance();
        TimeZone tz = calendar.getTimeZone();
        DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
        DateTime dateTime = new DateTime(calendar.getTimeInMillis(), jodaTz);
        DateFormat df = new SimpleDateFormat("yy_MM_dd"); // Just the year, with 2 digits
        String formattedYear = df.format(Calendar.getInstance().getTime());

        try {
            int i = 1;
            String fileName = "Toollogo_" + formattedYear + "_" + i + ".txt";
            File f = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), fileName);
            //File f = new File(rootPath + fileName);
            while (f.exists()) {
                fileName = "Toollogo_" + formattedYear + "_" + i + ".txt";
                f = new File(rootPath + fileName);
                i++;
            }
            f.createNewFile();
            FileOutputStream out = openFileOutput(fileName, Context.MODE_APPEND);

            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getData();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        switch (requestCode) {
            case REQUEST_LOGIN:
                if (RESULT_OK == resultCode) {

                    break;
                } else finish();
            default:
                break;
        }

    }

    private void initializeView() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mUser = (User) bundle.getSerializable(Const.ARG_LOGIN);
            assert mUser != null;
            mUserInfo.setText(mUser.getName());
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        mRefresh.setOnRefreshListener(this);
        mAdapter = new CountsAdapter(countList, this);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);
        mDateEnd = new DateTime();
        mDateStart = mDateEnd.minusDays(90);
        mDate.setText(
                mDateStart.getMonthOfYear() + "/" + mDateStart.getDayOfMonth() + "-" +
                        mDateEnd.getMonthOfYear() + "/" + mDateEnd.getDayOfMonth());

        rootPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/Download/";
        root = new File(rootPath);
        if (!root.exists()) {
            root.mkdirs();
        }

        getData();
    }

    private void getData() {
        try {

            File[] files = root.listFiles();
            Log.d("Files", "Size: " + files.length);

            countList.clear();
            for (int i = 0; i < files.length; i++) {
                Log.d("Files", "FileName:" + files[i].getName());
                String fname = files[i].getName();

                if (fname.contains("Toollogo_"))
                    countList.add(new Count(-1, "", files[i].getName(),
                        "", "0"));
            }
            mAdapter.notifyDataSetChanged();
            mRefresh.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
            mRefresh.setRefreshing(false);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth,
                          int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        TimeZone tz = Calendar.getInstance().getTimeZone();
        DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
        mDateStart = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0, 0, jodaTz);
        mDateEnd = new DateTime(yearEnd, monthOfYearEnd + 1, dayOfMonthEnd, 0, 0, 0, jodaTz);
        int days = daysBetween(mDateStart.toLocalDate(), mDateEnd.toLocalDate()).getDays();

        if (days < 0) {
            Toast.makeText(getApplicationContext(), "Эхлэх өдөр дуусахаасаа өмнө байх ёстой!",
                    Toast.LENGTH_LONG).show();
        } else {
            mDate.setText(String.valueOf(monthOfYear + 1) + "/" + dayOfMonth + "-" + String.valueOf(
                    monthOfYearEnd + 1) + "/" + dayOfMonthEnd);
            getData();
        }

    }

    @Override
    public void onRefresh() {
        getData();
    }

    @Override
    public void onClickOrder(int position) {
        Intent intent = new Intent(getApplicationContext(), CountOfflineActivity.class);
        intent.putExtra("count", countList.get(position));
        startActivityForResult(intent, 1);
    }

    @Override
    public void onLongClickOrder(final int position) {
        final ConfirmDialog dg = new ConfirmDialog(MainFileActivity.this,
                "Та уг тооллогыг устгах гэж байна! Үнэхээр устгах уу?");
        dg.show();
        Button btnYes = dg.getYesButton();
        if (btnYes != null)
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String fileName = countList.get(position).getName();
                    try {
                        File f = new File(rootPath + fileName);

                        if (f.exists()) {
                            f.delete();
                            if (f.exists()) {
                                f.getCanonicalFile().delete();
                                if (f.exists()) {
                                    getApplicationContext().deleteFile(f.getName());
                                }
                            }
                        }
                        getData();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dg.dismiss();

                }
            });
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mRefresh.setVisibility(show ? View.GONE : View.VISIBLE);
        mRefresh.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mRefresh.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgress.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
