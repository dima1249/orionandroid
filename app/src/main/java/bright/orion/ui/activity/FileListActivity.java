package bright.orion.ui.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import bright.orion.App;
import bright.orion.R;
import bright.orion.app.Const;
import bright.orion.model.Count;
import bright.orion.model.Sproduct;
import bright.orion.rest.Queries;
import bright.orion.ui.adapter.SproductAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FileListActivity extends AppCompatActivity implements SproductAdapter.ItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerSold)
    RecyclerView recyclerView;

    private GetCurrentCount mCountTask = null;
    private DeleteLine mDeleteTask = null;
    SproductAdapter mAdapter;
    private List<Sproduct> sProductList = new ArrayList<>();
    Count count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent res = new Intent();
                setResult(RESULT_OK, res);
                finish();
            }
        });
        mAdapter = new SproductAdapter(sProductList, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        count = (Count) getIntent().getSerializableExtra("count");
        mCountTask = new GetCurrentCount(count.getFullId());
        mCountTask.execute((Void) null);
    }

    @Override
    public void onBackPressed() {
        Intent res = new Intent();
        setResult(RESULT_OK, res);
        finish();
    }

    @OnClick(R.id.delete_btn)
    public void OnClickDelete() {
        for (int i = 0; i < sProductList.size(); i++) {
            if (sProductList.get(i).isChecked()) {
                final int finalI = i;
                new AlertDialog.Builder(this)
                        .setTitle("Сонгосон мөр устгах")
                        .setMessage("Та үнэхээр устгах уу ?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                mDeleteTask = new DeleteLine(sProductList.get(finalI).getLineId());
                                mDeleteTask.execute((Void) null);
                                mCountTask = new GetCurrentCount(count.getFullId());
                                mCountTask.execute((Void) null);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

            }
        }
    }

    @Override
    public void onClickOrder(int position) {
//        if (sProductList.get(position).isChecked()) {
//            sProductList.get(position).setChecked(false);
//        } else {
//            sProductList.get(position).setChecked(true);
//        }
//
//        mAdapter.notifyItemChanged(position);
    }

    @SuppressLint("StaticFieldLeak")
    public class GetCurrentCount extends AsyncTask<Void, Void, Intent> {

        private final String mFullId;
        private final long date;

        GetCurrentCount(String fullId) {
            this.mFullId = fullId;
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = calendar.getTimeZone();
            DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
            DateTime dateTime = new DateTime(calendar.getTimeInMillis(), jodaTz);
            this.date = dateTime.getMillis();
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.getCurrentCount();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setString(1, mFullId);
                    preStmt.setTimestamp(2, new Timestamp(date));
                    ResultSet rs = preStmt.executeQuery();
                    sProductList.clear();
                    ResultSetMetaData metadata = rs.getMetaData();
                    int columnCount = metadata.getColumnCount();
                    while (rs.next()) {
                        sProductList.add(new Sproduct(rs.getInt("line_id"), rs.getString("product_name"), rs.getString("qty_and_price")));
                        String row = "";
                        for (int i = 1; i <= columnCount; i++) {
                            if (i > 1) Log.e(" ", ", ");
                            String columnValue = rs.getString(i);
                            Log.e(columnValue, " " + metadata.getColumnName(i));
                        }
                    }
                    Log.e("HAHAHAHA--->", String.valueOf(sProductList.size()));
                    sProductList.add(new Sproduct(1, "hahah", "hehe"));
                    sProductList.add(new Sproduct(2, "hahah", "hehe"));
                    sProductList.add(new Sproduct(3, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                    sProductList.add(new Sproduct(4, "hahah", "hehe"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
                mAdapter.notifyDataSetChanged();
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class DeleteLine extends AsyncTask<Void, Void, Intent> {

        private final int mLineId;

        DeleteLine(int lineId) {
            this.mLineId = lineId;
        }

        @Override
        protected Intent doInBackground(Void... params) {
            Intent res = new Intent();
            Bundle data = new Bundle();
            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
                res.putExtras(data);
                return res;
            }

            try {
               App.reConnect(getApplicationContext());
                Connection con = App.getConnection();
                if (con == null) {
                    data.putString(Const.KEY_ERROR_MESSAGE, "Error in connection with SQL server");
                } else {
                    String query = Queries.deleteLine();
                    PreparedStatement preStmt = con.prepareStatement(query);
                    preStmt.setInt(1, mLineId);
                    ResultSet rs = preStmt.executeQuery();
                }
            } catch (SQLException e) {
                e.printStackTrace();
                data.putString(Const.KEY_ERROR_MESSAGE, e.toString());
            }
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent res) {
            mCountTask = null;
            if (res.hasExtra(Const.KEY_ERROR_MESSAGE)) {
                Bundle bundle = res.getExtras();
                if (bundle != null)
                    Toast.makeText(getApplicationContext(),
                            res.getExtras().getString(Const.KEY_ERROR_MESSAGE),
                            Toast.LENGTH_LONG).show();
            } else {
            }
        }

        @Override
        protected void onCancelled() {
            mCountTask = null;
        }
    }

}
