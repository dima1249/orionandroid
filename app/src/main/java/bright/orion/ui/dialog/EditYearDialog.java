package bright.orion.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import bright.orion.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditYearDialog extends Dialog {
    @Nullable
    @BindView(R.id.soldYear)
    Spinner dialogSpinner;
    @Nullable
    @BindView(R.id.yes)
    Button yesButton;
    @Nullable
    @BindView(R.id.closeBtn)
    Button closeBtn;

    public EditYearDialog(Context context, ArrayAdapter<String> yearAdapter, String defYear) {
        super(context);
        setContentView(R.layout.dialog_edit_year);
        ButterKnife.bind(this, this);
        if (dialogSpinner != null) {
            dialogSpinner.setAdapter(yearAdapter);
            for (int i = 0; i < yearAdapter.getCount(); i++) {
                if (yearAdapter.getItem(i).equals(defYear))
                    dialogSpinner.setSelection(i);
            }
        }
    }

    @OnClick(R.id.closeBtn)
    public void onClose() {
        dismiss();
    }

    @Nullable
    public String getYear() {
        if (dialogSpinner != null)
            return dialogSpinner.getSelectedItem().toString();
        else return "";
    }

    @Nullable
    public Button getYesButton() {
        return yesButton;
    }

    @Nullable
    public Button getNoButton() {
        return closeBtn;
    }

}
