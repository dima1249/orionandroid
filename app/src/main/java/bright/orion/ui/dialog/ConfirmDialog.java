package bright.orion.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.TextView;

import bright.orion.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmDialog extends Dialog {
    @Nullable
    @BindView(R.id.message)
    TextView dialogMessage;
    @Nullable
    @BindView(R.id.yes)
    Button yesButton;
    @Nullable
    @BindView(R.id.closeBtn)
    Button closeBtn;

    public ConfirmDialog(Context context, String message) {
        super(context);
        setContentView(R.layout.dialog_confirm);
        ButterKnife.bind(this, this);
        assert dialogMessage != null;
        dialogMessage.setText(message);
        //getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

    @OnClick(R.id.closeBtn)
    public void onClose() {
        dismiss();
    }

    @Nullable
    public Button getYesButton() {
        return yesButton;
    }

    @Nullable
    public Button getNoButton() {
        return closeBtn;
    }

}
