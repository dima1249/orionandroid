package bright.orion.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import bright.orion.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditNumberDialog extends Dialog {
    @Nullable
    @BindView(R.id.message)
    EditText dialogMessage;
    @Nullable
    @BindView(R.id.yes)
    Button yesButton;
    @Nullable
    @BindView(R.id.closeBtn)
    Button closeBtn;

    public EditNumberDialog(Context context, String number) {
        super(context);
        setContentView(R.layout.dialog_edit_number);
        ButterKnife.bind(this, this);
        if (dialogMessage != null) {
            if (number != null)
                dialogMessage.setText(number);
            dialogMessage.requestFocus();
        }
        Window window = getWindow();
        if (window != null)
            window.setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @OnClick(R.id.closeBtn)
    public void onClose() {
        dismiss();
    }

    @Nullable
    public String getNumber() {
        if (dialogMessage != null)
            return dialogMessage.getText().toString();
        else return "";
    }

    @Nullable
    public Button getYesButton() {
        return yesButton;
    }

    @Nullable
    public Button getNoButton() {
        return closeBtn;
    }

}
