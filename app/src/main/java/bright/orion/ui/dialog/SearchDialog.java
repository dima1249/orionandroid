package bright.orion.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import bright.orion.R;
import bright.orion.model.Product;
import bright.orion.ui.adapter.FilterAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class SearchDialog extends Dialog implements FilterAdapter.ProductsAdapterListener {
    @Nullable
    @BindView(R.id.search_text)
    EditText searchTxt;
    @Nullable
    @BindView(R.id.search_btn)
    ImageButton searchBtn;
    @BindView(R.id.recycler_view)
    RecyclerView mRecycler;
    @Nullable
    @BindView(R.id.closeBtn)
    Button closeBtn;

    private List<Product> productList = new ArrayList<>();
    private FilterAdapter mAdapter;
    ProductClickListener productClickListener;

    public SearchDialog(Context context, List<Product> productList, ProductClickListener productClickListener) {
        super(context);
        setContentView(R.layout.dialog_products);
        ButterKnife.bind(this, this);

        this.productList = productList;
        this.productClickListener = productClickListener;

        mAdapter = new FilterAdapter(context, productList, this);
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(context));
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    @OnTextChanged(R.id.search_text)
    public void onTextChanged(CharSequence query) {
        mAdapter.getFilter().filter(query);
    }

    @OnClick(R.id.closeBtn)
    public void onClose() {
        dismiss();
    }

    @Override
    public void onProductSelected(Product product) {
        productClickListener.onClickProduct(product);
        dismiss();
    }

    public interface ProductClickListener {
        void onClickProduct(Product product);
    }
}
