package bright.orion.ui.adapter;

import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bright.orion.R;
import bright.orion.model.Count;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by orgil on 12/12/2017.
 */

public class CountsAdapter extends RecyclerView.Adapter<CountsAdapter.OrderVh> {

    private List<Count> countList;
    final ItemClickListener listener;

    public CountsAdapter(List<Count> countList, ItemClickListener listener) {
        this.countList = countList;
        this.listener = listener;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderVh(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_count, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderVh holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return countList.size();
    }

    class OrderVh extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ConstraintLayout mContainer;
        @BindView(R.id.containerOrder)
        ConstraintLayout mContainerOrder;
        @BindView(R.id.count_name)
        TextView mCountName;
        @BindView(R.id.state)
        TextView mState;
        @BindView(R.id.total_price)
        TextView mTotalPrice;


        Count count;

        public OrderVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setData(final int position) {
            count = countList.get(position);

            mCountName.setText(count.getName());
            mTotalPrice.setText("Тоолсон үнийн дүн: " + count.getTotalPrice());


            if (count.getState().equals("draft")) {
                mState.setText("Төлөв: Ноорог");
                mCountName.setTypeface(null, Typeface.BOLD);
                mState.setTypeface(null, Typeface.BOLD);
            } else if (count.getState().equals("confirmed")) {
                mState.setText("Төлөв: Батлагдсан");
                mCountName.setTypeface(null, Typeface.NORMAL);
                mState.setTypeface(null, Typeface.NORMAL);
            }
            if (count.getId() == -1) {
                mState.setVisibility(View.GONE);
                mTotalPrice.setVisibility(View.GONE);
                mCountName.setTypeface(null, Typeface.NORMAL);
            }

            mContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickOrder(position);
                }
            });
            mContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onLongClickOrder(position);
                    return false;
                }
            });
        }
    }

    public interface ItemClickListener {
        void onClickOrder(int position);

        void onLongClickOrder(int position);
    }
}
