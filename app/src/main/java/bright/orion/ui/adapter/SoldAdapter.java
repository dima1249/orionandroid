package bright.orion.ui.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bright.orion.R;
import bright.orion.model.Ballon;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by orgil on 12/12/2017.
 */

public class SoldAdapter extends RecyclerView.Adapter<SoldAdapter.SoldVh> {

    private List<Ballon> mBallonList = new ArrayList<>();
    private final ItemClickListener mListener;

    public SoldAdapter(List<Ballon> mBallonList, ItemClickListener mListener) {
        this.mBallonList = mBallonList;
        this.mListener = mListener;
    }

    @Override
    public SoldVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SoldVh(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sold_ballon, parent, false));
    }

    @Override
    public void onBindViewHolder(SoldVh holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mBallonList.size();
    }

    class SoldVh extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ConstraintLayout mContainer;
        @BindView(R.id.soldTypeBallon)
        TextView mAddTypeBallon;
        @BindView(R.id.soldYear)
        TextView mSoldYear;
        @BindView(R.id.addNumberBallon)
        EditText mAddNumberBallon;
        @BindView(R.id.removeBtn)
        ImageButton mRemoveBtn;

        Ballon ballon;

        public SoldVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setData(final int position) {
            ballon = mBallonList.get(position);

            mAddTypeBallon.setText(ballon.getType());
            mAddNumberBallon.setText(ballon.getNumber());
            mAddNumberBallon.setText(ballon.getNumber());
            mSoldYear.setText(ballon.getYear());
            mRemoveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClickRemoveSold(position);
                }
            });

            mAddNumberBallon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onSetBallonNumber(position);
                }
            });

            mSoldYear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onSetBallonYear(position);
                }
            });
        }

    }

    public interface ItemClickListener {
        void onClickRemoveSold(int position);

        void onSetBallonNumber(int position);

        void onSetBallonYear(int position);
    }
}
