package bright.orion.ui.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import bright.orion.R;
import bright.orion.model.Sproduct;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by orgil on 12/12/2017.
 */

public class SproductAdapter extends RecyclerView.Adapter<SproductAdapter.OrderVh> {

    private List<Sproduct> countList;
    final ItemClickListener listener;
    private int selectedPosition = -1;

    public SproductAdapter(List<Sproduct> countList, ItemClickListener listener) {
        this.countList = countList;
        this.listener = listener;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderVh(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sprocuct, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderVh holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return countList.size();
    }

    class OrderVh extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ConstraintLayout mContainer;
        @BindView(R.id.containerOrder)
        ConstraintLayout mContainerOrder;
        @BindView(R.id.check_box)
        CheckBox mCheckbox;
        @BindView(R.id.product_name)
        TextView mProductName;
        @BindView(R.id.description)
        TextView mDescription;

        Sproduct count;

        public OrderVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setData(final int position) {
            count = countList.get(position);

            mProductName.setText(count.getName());
            mDescription.setText(count.getDescription());


            if (count.isChecked()) {
                selectedPosition = position;
                mCheckbox.setChecked(true);
            } else {
                mCheckbox.setChecked(false);
            }

            mContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickOrder(position);
                }
            });
            mCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        count.setChecked(true);
                        if (selectedPosition >= 0 && selectedPosition != position) {
                            countList.get(selectedPosition).setChecked(false);
                            notifyItemChanged(selectedPosition);
                        }
                        selectedPosition = position;
                    } else {
                        count.setChecked(false);
                    }
                }
            });
        }

    }

    public interface ItemClickListener {
        void onClickOrder(int position);
    }
}
