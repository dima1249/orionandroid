package bright.orion.ui.adapter;

import android.annotation.SuppressLint;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bright.orion.R;
import bright.orion.model.Ballon;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by orgil on 12/12/2017.
 */

public class ReturnAdapter extends RecyclerView.Adapter<ReturnAdapter.RetVh> {

    private List<Ballon> ballonList = new ArrayList<>();
    final ItemClickListener listener;

    public ReturnAdapter(List<Ballon> ballonList, ItemClickListener listener) {
        this.ballonList = ballonList;
        this.listener = listener;
    }

    @Override
    public RetVh onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RetVh(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_ret_ballon, parent, false));
    }

    @Override
    public void onBindViewHolder(RetVh holder, int position) {
        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return ballonList.size();
    }

    class RetVh extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ConstraintLayout mContainer;
        @BindView(R.id.retBallonInfo)
        TextView mRetBallonInfo;
        @BindView(R.id.removeBtn)
        ImageButton mRemoveBtn;

        Ballon ballon;

        public RetVh(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("DefaultLocale")
        void setData(final int position) {
            ballon = ballonList.get(position);
            mRetBallonInfo.setText(ballon.getInfo());
            mRemoveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickRemoveRet(position);
                }
            });
        }
    }

    public interface ItemClickListener {
        void onClickRemoveRet(int position);
    }
}
